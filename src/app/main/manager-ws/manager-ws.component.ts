import {Component, OnInit, ViewChild} from '@angular/core';


@Component({
    selector: 'app-manager-ws',
    templateUrl: './manager-ws.component.html',
    styleUrls: ['./manager-ws.component.scss']
})
export class ManagerWsComponent implements OnInit {

    departmentList = [
        {
            name: 'Онкология'
        },
        {
            name: 'Кордиология'
        },
        {
            name: 'Урология'
        },
        {
            name: 'Педиатрия'
        },
        {
            name: 'Терапия'
        },
        {
            name: 'Травматология'
        }];
    orders = [
        {
            name: 'Газет',
            series: [
                {
                    name: '1980',
                    value: 97
                }
            ]
        }
    ];

    public colorScheme = {
        domain: ['#999']
    };
    public autoScale = true;

    constructor() {

    }

    ngOnInit()
        :
        void {

        this.orders = this.addRandomValue('orders');
    }

    public addRandomValue(param) {
        switch (param) {
            case 'orders':
                for (let i = 1; i < 30; i++) {
                    this.orders[0].series.push({name: '' + (1980 + i), value: Math.ceil(Math.random() * 100)});
                }
                return this.orders;
        }
    }

}
