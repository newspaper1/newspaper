export class ServiceCommonConstant {

    public static readonly baseUrl = '/amc';
    public static readonly gatewayModuleUrl = ServiceCommonConstant.baseUrl + '/auth';
    public static readonly adminModuleUrl = ServiceCommonConstant.baseUrl + '/admin';
    public static readonly coreModuleUrl = ServiceCommonConstant.baseUrl + '/core';

}
